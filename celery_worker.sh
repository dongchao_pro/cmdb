#!/bin/sh

# 启动: ./celery_worker.sh start  
# 停止: ./celery_worker.sh stop  
cur_dir=`dirname $0`/
cd $cur_dir
proj="website"

function color_msg() {
  case "$1" in
  "error"|"red")
    echo -e "\e[1;31m$2\e[0m"
    ;;
  "warn"|"yellow")
    echo -e "\e[1;33m$2\e[0m"
    ;;
  "success"|"green")
    echo -e "\e[1;32m$2\e[0m"
    ;;
  esac
}

## 终端启动
# ps uax |grep celery|grep worker |wc
# celery -A webapp   worker -l info 
# ps aux |grep worker|grep celery|grep -v grep|awk '{print $2}'|xargs kill -9

## celery multi 管理
# celery multi start w1 -A proj -l info
# celery  multi restart w1 -A proj -l info
# 异步关闭 立即返回
# celery multi stop w1 -A proj -l info
# 等待关闭操作完成
# celery multi stopwait w1 -A proj -l info


case $1 in                                        
  start)
    color_msg green "## 启动服务 [$proj]"
     celery multi start w2 -A $proj  -l info;; 
  stop) 
     color_msg green "## 关闭服务 [$proj]"
     celery multi stopwait w2 -A $proj -l info;; 
  *) echo "require start|stop" ;;     
esac