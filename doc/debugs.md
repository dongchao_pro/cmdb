<!--
 * @Author: admin@attacker.club
 * @Date: 2022-07-19 22:39:07
 * @LastEditTime: 2022-07-19 22:59:54
 * @Description:
-->

# 调试

## 调试 shell

```bash
python manage.py shell
import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')
django.setup()

from asset.models import Host
Host.objects.all()
```

## Mac 安装 python-ldap

如果不想安装 xcode,那么可以通过重置系统默认开发工具路径

```bash
sudo xcode-select -r
xcode-select --switch /Library/Developer/CommandLineTools
xcode-select -p

cd python-ldap-3.4.2
python setup.py install
```
