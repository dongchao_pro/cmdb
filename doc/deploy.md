<!--
 * @Author: admin@attacker.club
 * @Date: 2022-07-19 09:04:27
 * @LastEditTime: 2022-07-25 16:37:27
 * @Description:
-->

# 部署文档

## 前端

```vue
npm install npm run build:prod
```

## 后端

```bash
python3  -m venv env
source env/bin/activate
pip install -i https://mirrors.aliyun.com/pypi/simple -r requirements.txt

# pip install -i  http://mirrors.aliyun.com/pypi/simple  --trusted-host mirrors.aliyun.com    -r requirements.txt

# django-admin startproject website . # 建立项目
# python manage.py startapp # 添加新应用
# python manage.py makemigrations
python manage.py makemigrations  oauth  asset  app
# 为改动models创建迁移记录
python manage.py migrate  # 同步数据库

# python manage.py  createsuperuser
# 建立后台管理员帐号
```

### 导入默认数据

```bash
python manage.py loaddata doc/dumpdata.json
# 导入命令

# python manage.py dumpdata >  dumpdata.json
# 导出命令
```

### 启动服务

```bash
python manage.py runserver
python manage.py runserver  0.0.0.0:8000
```

```bash
## docker
docker run -d -p8001:8000 -e DB_NAME='cmdb' -e DB_HOST='127.0.0.1' -e DB_PASSWORD='123456' lghost/cmdb
```

### 登录权限

> 账号 admin,test,visitor / 密码 123456

## 接口地址

```text
admin后台:
http://127.0.0.1:8000/admin

api:
http://127.0.0.1:8000/swagger/
http://127.0.0.1:8000/docs/
```
