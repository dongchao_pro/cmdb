'''
Author: admin@attacker.club
Date: 2019-04-17 22:41:30
LastEditTime: 2022-08-06 23:58:12
Description: 
'''
from __future__ import absolute_import, unicode_literals
import os
from celery import Celery, platforms
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')
REDIS_CONN = f'redis://:{settings.REDIS_PWD}@{settings.REDIS_HOST}:{settings.REDIS_PORT}'


# 任务存储
broker_url = f"{REDIS_CONN}/8"
# 结果存储
result_backend = f"{REDIS_CONN}/9"


## 项目名，注册Celery的APP
app = Celery('devops', 
backend=result_backend, 
broker=broker_url,
CELERYD_CONCURRENCY = 2 ,    # celery worker并发数
CELERYD_MAX_TASKS_PER_CHILD = 3 #   每个worker最大执行任务数
)
## 绑定配置文件
app.config_from_object('django.conf:settings', namespace='CELERY')
## 自动发现各个app下的tasks.py文件
app.autodiscover_tasks()

## 允许root 用户运行celery
platforms.C_FORCE_ROOT = True

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
