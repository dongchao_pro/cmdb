# cmdb 管理系统

## 环境

![Python](https://img.shields.io/badge/python-3.9-blue.svg?style=plastic)
![Django](https://img.shields.io/badge/django-3.2.9-blue.svg?style=plastic)
![DRF](https://img.shields.io/badge/djangorestframework-3.12.4-blue.svg?style=plastic)
![Simpleui](https://img.shields.io/badge/simpleui-3.0-brightgreen.svg?style=plastic)
![vue-element-admin](https://img.shields.io/badge/vue--element--admin-4.0-green.svg?style=plastic)

## 开发语言与框架

```text
前端Web框架： vue-element-admin
后端Web框架： Django REST framework
```

## 功能说明

![cmdb](http://assets.processon.com/chart_image/60a63cd30791296d60a5b88b.png)

**action** :

- 支持 ldap 认证登录
- 基于 rbac 角色权限管理
- 支持 ssh 密码/密钥方式 web 终端访问
- 容器化构建镜像
- 新增 jenkins+kubernets 部署文件
- 新增 WebHook 触发自动构建任务

## simpleui admin 后台模板

![simpleui](uploads/image/simpleui.png)

## 资产管理

### ssh

![ssh](uploads/image/ssh.jpg)

### 主机列表

![host](uploads/image/host.png)

### webssh

![webssh](uploads/image/webssh.png)

## 应用发布

### 发布流程

![cmdb-deploy](uploads/image/cmdb-deploy.png)

### 应用配置登记

![app](uploads/image/app.png)

### cmdb 数据 + Jenkins + python 发布脚本

![jenkins](uploads/image/jenkins.png)

![jenkins-demo](uploads/image/jenkins-demo.png)


### 启动项目

依赖:
> mysql , redis , openladp(可选)

```bash
docker run --name cmdb \
-d -p8000:8000  \
-e DB_NAME=${DB_NAME} \
-e DB_HOST=${DB_HOST} \
-e DB_PASSWORD=${DB_PASSWORD} \
-e DB_PORT=${DB_PORT} \
-e REDIS_HOST=${REDIS_HOST} \
-e REDIS_PWD=${REDIS_PWD} \
-e REDIS_PORT=${REDIS_PORT} \
-e AUTH_LDAP_SERVER_URI='ldap://opsbase.cn' \
-e AUTH_LDAP_BIND_DN='cn=admin,dc=opsbase,dc=cn' \
-e LDAP_ADMIN_PASSWORD='123456' \
lghost/devops:v1

=================================
docker run --name cmdb \
-d -p8000:8000  \
-e DB_NAME='cmdb' \
-e DB_HOST='192.168.0.102' \
-e DB_PASSWORD='123456' \
-e DB_PORT=3306 \
-e REDIS_HOST='192.168.0.30' \
-e REDIS_PWD='123456' \
-e REDIS_PORT=6379 \
-e AUTH_LDAP_SERVER_URI='ldap://opsbase.cn' \
-e AUTH_LDAP_BIND_DN='cn=admin,dc=opsbase,dc=cn' \
-e LDAP_ADMIN_PASSWORD='123456' \
lghost/cmdb
==========================================

docker exec -it cmdb sh # 登录
docker stop  cmdb && docker rm cmdb # 删除
tail  logs/access.log -f # 查看日志
```



> 需要先建库

### 初始化导入默认数据

```bash
docker exec -it  cmdb  python init.py
```

### 启动celery任务

```
celery -A website worker -l info

# 脚本启动: ./celery_worker.sh start  
# 脚本停止: ./celery_worker.sh stop  
```


### 登录账号

```text
http://127.0.0.1:8000

visitor / 123456
admin / 123456
test / 123456
```

### 其他接口地址

> http://127.0.0.1:8000/admin

> http://127.0.0.1:8000/swagger/

> http://127.0.0.1:8000/docs

> http://127.0.0.1:8000/prometheus/metrics

### Jenkins CI 生成主机选项

```bash
python utils/get_application_cluster_ip.py  Pro.GM.zsj.web-game
```

```text
[pro-skyvpn-jar-web1]-192.168.1.124
[pro-us-host2]-192.168.1.1
[pro-hk-host1]-192.168.0.30
```

## Demo

<http://cmdb.pod.opsbase.cn/>

> visitor / 123456

---

扫码加入 **运维大本营**

![qq](uploads/image/qq.png)

## License

项目基于 GPLv3 协议， 详细请参考 [LICENSE](LICENSE) 。
