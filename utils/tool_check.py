#!/usr/bin/env python
# coding=utf-8
'''
author: 以谁为师
site: opsbase.cn
Date: 2022-03-13 21:35:02
LastEditTime: 2022-03-29 12:33:14
Description: 


REDIS_STATUS = PortScan(REDIS_HOST,REDIS_PORT)
if REDIS_STATUS:
'''

import socket

   
def PortScan(address, port):
    try:
        socket.setdefaulttimeout(3)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex((address,int(port)))
        sock.close()

        if result == 0:
            return True
        else:
            return False
        # conn = socket.socket()
        # conn.connect((ip,int(port)))
        # conn.close()
        # return True
        
    except:
        return False

if __name__ == '__main__':
    res = PortScan('opsbase.cn', '3306')
    if res:
        print('ok')
