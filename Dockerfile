# version:2
FROM harbor.opsbase.cn/public/cmdb


WORKDIR /home/app
COPY . /home/app
RUN pip --no-cache-dir install -i http://mirrors.aliyun.com/pypi/simple --trusted-host mirrors.aliyun.com -r requirements.txt



EXPOSE 8000
CMD ["/bin/sh","run.sh"]

# RUN  python manage.py makemigrations  &&  python manage.py migrate
# CMD [ "python", "./manage.py", "runserver", "0.0.0.0:8000"]

# docker build -t lghost/cmdb:latest . -f Dockerfile # 构建
# docker build -t lghost/cmdb:v1 . -f Dockerfile-CI # 基于基础包构建
# docker push lghost/cmdb:latest # 提交到dockerhub

## 私有库tag
# docker tag lghost/cmdb:latest harbor.opsbase.cn/public/cmdb:latest

## 简洁启动
# docker run  -d -p8000:8000  --name cmdb  lghost/cmdb:latest

## 初始化数据
# docker exec -it cmdb python init.py

