from oauth.models import *
import django_filters
from django.db.models import Q



class OauthFilter(django_filters.rest_framework.FilterSet):

    search = django_filters.CharFilter(method='hybridFilter', help_text='混合搜索')

    def hybridFilter(self, queryset, name, value):
        return UserProfile.objects.filter(Q(name__contains=value) | Q(username__contains=value)| Q(mobile__contains=value)| Q(email__contains=value))
    class Meta:
        model = UserProfile
        fields = ['search']

    # &search='Logan.Li'