
from django.urls import path, include
from oauth.views import oauth
from oauth.views import departments
from oauth.views import permissions
from oauth.views import roles

from rest_framework.routers import DefaultRouter
router = DefaultRouter()   # 实例化路由控制对象

router.register(r'users', oauth.UsersViewSet, basename="users")  # 用户管理
router.register(r"permissions",permissions.PermissionsViewSet,basename="permissions")  # 权限管理
router.register(r'roles', roles.RolesViewSet, basename="roles")  # 角色管理
router.register(r'departments', departments.DepartmentsViewSet, basename="departments")  # 部门管理

urlpatterns = [

    path('permissions/methods/', permissions.PermissionsMethodsAPIView.as_view()),  # 权限models方法列表信息
    path('users/<int:pk>/permissions/', oauth.PermissionsAPIView.as_view()),  # 用户权限ID列表
    path('users/reset-password/<int:pk>/', oauth.ResetPasswordAPIView.as_view()),  # 重置密码
    path('', include(router.urls))

]
