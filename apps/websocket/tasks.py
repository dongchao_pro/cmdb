from __future__ import absolute_import, unicode_literals
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from celery import shared_task
from websocket.ssh import SSH

@shared_task
def task_add( channel_name):
    self.ssh = SSH(websocker, message=self.message)

    # 通过channels_layers从websocketconsumer函数外部向channels发送消息，
    channel_layer = get_channel_layer()
    # 异步转同步使用async_to_sync方法，channel_name为频道名，这里直接用的websocketconsumer自带的channel_name属性，type发送的方法，send.message对应前面MyConsumer里的send_message，这里channels是对.进行了内部转化为_，没啥特殊理解的，message就是你要发送的消息
    async_to_sync(channel_layer.send)(
                        channel_name,
                        {"type": "send.message","message": ''}
                    )