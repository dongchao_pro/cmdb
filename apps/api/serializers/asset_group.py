from rest_framework import serializers
from asset.models import *
from .mixins  import *
import json

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = "__all__"