#!/usr/bin/python
# -*- coding: utf-8 -*-

# describe：CMDB采集脚本，对python版本和执行用户没要求
# 解决python执行编码问题
import sys
try:
    reload(sys) 
    sys.setdefaultencoding('utf8')
except:
    pass

import socket, fcntl, struct
from datetime import datetime, date, timedelta
import os, json

try:
    from urllib import request
except:
    import urllib2 as request
import logging

# 当前目录
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# 日志配置
log_file = os.path.join(BASE_DIR, "collect.log")
logging.basicConfig(level=logging.INFO,filename=log_file,format="%(asctime)s - [%(levelname)s] %(message)s")

class GetData():
    def __init__(self):
        self.result = {}
    # 解析文件，为获取CPU和内存使用
    def parse_file(self, file, name):
        with open(file) as f:
            for line in f.readlines():
                key, value = line.split(":")
                key = key.strip()
                value = value.strip()
                if key == name:
                    return value
    def hostname(self):
        hostname = socket.gethostname()
        # hostname_backup = '/tmp/.hostname'
        # if os.path.isfile(hostname_backup) and os.path.getsize(hostname_backup) != 0:
        #     with open(hostname_backup) as f:
        #         hostname = f.read().strip()
        # else:
        #     with open(hostname_backup, 'w') as f:
        #         f.write(hostname)
        return hostname
    def machine_type(self):
        result = os.popen("dmesg |grep -i virtual |grep -ci hardware")
        if int(result.read()) >= 1:
            type = "physical_machine" # 物理机
        else:
            result = os.popen("dmesg |grep -i virtual |grep -ci kvm")
            if int(result.read()) >= 1:
                type = "cloud_host" # 云主机
            else:
                type = "virtual_machine" # 虚拟机
        return type
    
    # 获取系统版本，兼容centos7和Ubuntu
    def os_version(self):
        with open("/etc/issue") as f:
            if f.readline().strip() == "\S":
                with open("/etc/redhat-release") as f:
                    os_version = f.readline().strip()
            else:
                os_version = f.readline().strip()
        return os_version
        
    # 系统启动时间，先作为服务器上架时间
    # def system_up_time(self):
    #     with open("/proc/uptime") as f:
    #         s = f.read().split(".")[0] # 启动有多少秒
    #     up_time = datetime.now() - timedelta(seconds=float(s)) # 当前时间减去启动秒
    #     return date.strftime(up_time, '%Y-%m-%d')

    def floatingip(self):
        private_ip = self.private_ip()
        ip_api_url = ['http://ifconfig.me/ip','http://ifconfig.io']
        ip_list = []
        try:
            req = request.Request(url=ip_api_url[0])
            res = request.urlopen(req)
            ip = res.read().decode()
        except Exception as e:
            req = request.Request(url=ip_api_url[0])
            res = request.urlopen(req)
            ip = res.read().decode()
        
        if ip in private_ip:
            return ip_list
        else:
            ip_list.append(ip)
            return ip_list

    def private_ip(self):
        nic_prefix = ['eth', 'en', 'em'] # 常见网卡名前缀
        ip_list = []
        with open("/proc/net/dev") as f:
            for s in f.readlines():
                name = s.split(':')[0].strip()
                for p in nic_prefix:
                    if name.startswith(p):
                        result = os.popen("ip addr show %s |awk -F'[ /]' '/inet /{print $6}'" %name)
                        ip_list.append(result.read().strip())   
        return ip_list
    def cpu_num(self):
        cpu = self.parse_file("/proc/cpuinfo", "cpu cores")
        logging.info("cpu: {}".format(cpu))
        return "%s核" %cpu
    def cpu_model(self):
        model = self.parse_file("/proc/cpuinfo", "model name")
        return model
    def memory(self):
        total = self.parse_file("/proc/meminfo", "MemTotal")
        total = round(float(total.split()[0]) / 1024 / 1024, 1) # 转GB单位
        logging.info("total: {}".format(total))
        return "%sG" %total
    def disk(self):
        disk = []
        result = os.popen("lsblk |awk '$6~/disk/{print $1,$4,$5}'")
        for d in result.read().strip().split('\n'):
            d = d.split()
            device = d[0]
            size = d[1]
            type = "HDD" if d[2] == 0 else "SSD"
            disk.append({'device': '/dev/%s' %device, 'size': size, 'type': type})
            logging.info("disk: {}".format(disk))
        return disk
    def get_all(self):
        """
        这里字段必须与API对应
        """
        try:
            disk = self.disk()
            disk = "{0} {1}".format(disk[-1]['device'],disk[-1]['size'])
            cpu = "{0} {1}".format(self.cpu_num(),self.cpu_model())
            if self.floatingip():
                floatingip=self.floatingip()[0]
            else:
                floatingip = ''

            self.result = {
                "hostname":self.hostname(),
                "os_version": self.os_version(),
                "floatingip": floatingip,
                "ip": self.private_ip()[0],
                "cpu": cpu,
                "memory": self.memory(),
                "disk": disk ,
                }
            json_data = json.dumps(self.result)
            logging.info(json_data)
            return json_data
        except Exception as e:
            json_data = json.dumps(e)
            logging.info(json_data)
            return json_data

if __name__ == "__main__":
    data = GetData()
    try:
        print(data.get_all())
    except Exception as e:
        print(e)
        # result = {'code': 500, 'msg': '采集脚本执行失败！错误：%s' %e}
        # print(json.dumps(result))
