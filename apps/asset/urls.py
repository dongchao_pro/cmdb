
from django.urls import path,re_path, include
from .views import host
from .views import ssh
from .views import platform
from .views import group
from .views import region
from .views import aliyunCloud

from rest_framework.routers import DefaultRouter
router = DefaultRouter()
router.register(r'platform/account', platform.PlatformAccountViewSet, basename="platform")  
router.register(r'ssh', ssh.SSH_accountViewSet, basename="ssh")
router.register(r'region', region.RegionSerializerViewSet, basename="region")
router.register(r'platforms', platform.PlatformViewSet, basename="platform")
router.register(r'groups', group.GroupSerializerViewSet, basename="groups")


urlpatterns = [

    path('servers/multiple_delete/',host.HostsMultipleDestroy.as_view()),
    path('servers/',host.HostsViewSet.as_view()),  
    re_path('servers/(?P<pk>\d+)/$',host.HostsDetailView.as_view()),
    path('servers/type/', host.ServerTypeAPIView.as_view()),
    path('servers/status/', host.ServerStatusAPIView.as_view()),
    path('ssh/type/', ssh.SSH_accountChoiceAPIView.as_view()),
    path('servers/departmentList/', host.ServerDepartmentListAPIView.as_view()),
    path('servers/ipList/', host.ServerIPListAPIView.as_view()),
    path('servers/regionList/', host.ServerRegionListAPIView.as_view()),
    path('servers/admin/', host.AssetsAdminListAPIView.as_view()),
    path('aliyun_cloud/update/', aliyunCloud.AliyunCloudUpdateView.as_view()),
    path('aliyun_cloud/region/', aliyunCloud.AliyunCloudRegionView.as_view()),
    path('host_collect/',host.HostCollectView.as_view()),
    path('', include(router.urls))


]
