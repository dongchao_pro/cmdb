from django.db import models
# from datetime import date
from django.utils import timezone 
import  datetime 

class PlatformAccount(models.Model):
    """ 平台账号 """
    name = models.CharField(max_length=255, blank=True, null=True, verbose_name="账户")
    platform_id = models.ForeignKey("Platform", db_column='platform_id',  blank=True, null=True, on_delete=models.SET_NULL)
    accesskey = models.CharField( max_length=64,default='', verbose_name='AccessKey ID')
    secret = models.CharField( max_length=64,default='', verbose_name='Secret ID')
    gmt_create = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    gmt_modified = models.DateTimeField(auto_now=True, verbose_name="编辑时间")
    desc =  models.CharField(max_length=50, blank=True, null=True, verbose_name="描述")
    class Meta:
        # db_table = "cmdb_asset_platform_account"
        verbose_name = '平台账号'
        verbose_name_plural = verbose_name
        ordering = ['id']
        # unique_together = (('accesskey', 'secret'),)
    def __str__(self):
        return self.name


class Platform(models.Model):
    """云平台()"""
    name = models.CharField(db_column='name', max_length=64,
                            unique=True, verbose_name='名称')
    desc =  models.CharField(max_length=50, blank=True, null=True, verbose_name="描述")
    index = models.IntegerField(default=99, verbose_name='分类排序')
    class Meta:
        # db_table = "cmdb_asset_platform"
        verbose_name = '平台'
        verbose_name_plural = verbose_name
        ordering = ['index']
    def __str__(self):
        return self.name


class Region(models.Model):
    """区域运营商区域"""
    country = models.CharField(db_column='country', max_length=50, blank=True, null=True, verbose_name="国家")
    city = models.CharField(db_column='city', max_length=50,blank=True, null=True, verbose_name="城市")
    region = models.CharField(db_column='region', max_length=50, blank=True, null=True, verbose_name="区域")
    gmt_create = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    gmt_modified = models.DateTimeField(auto_now=True, verbose_name="编辑时间")
    desc =  models.CharField(max_length=50, blank=True, null=True, verbose_name="描述")
    class Meta:
        # db_table = "cmdb_asset_region"
        verbose_name = '区域'
        verbose_name_plural = verbose_name
        ordering = ['-country']
    def __str__(self):
        return self.region

class SSH_account(models.Model):
    """ SSH账号 """
    
    authentication_type_choice = (

        ('Text', '密码'),
        ('Key', '密钥'),
    )
    name = models.CharField(
        db_column='name', max_length=64,default='',verbose_name='名称')
    ssh_port = models.PositiveIntegerField(default='22',blank=True,null=True,verbose_name='端口')
    ssh_user = models.CharField(
        db_column='ssh_user', max_length=20, blank=True, null=True, verbose_name="用户名")
    authentication_type = models.CharField(choices=authentication_type_choice, db_column='authentication_type',
                                           max_length=20, blank=True, null=True, verbose_name="密码类型")
    password = models.CharField(max_length=20,null=True, blank=True, verbose_name='密码')
    private = models.TextField(null=True, blank=True, verbose_name="私钥pem")
    public = models.TextField(null=True, blank=True, verbose_name="公钥key")
    is_secret = models.BooleanField(db_column='is_secret', default=False,  verbose_name="加密")
    is_manager = models.BooleanField(db_column='is_manager', default=False,  verbose_name="管理员")
    index = models.IntegerField(default=99, verbose_name='排序')
    gmt_create = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    gmt_modified = models.DateTimeField(auto_now=True, verbose_name="编辑时间")
    desc =  models.CharField(max_length=50, blank=True, null=True, verbose_name="描述")
    class Meta:
        # db_table = "cmdb_asset_ssh"
        verbose_name = 'SSH'
        verbose_name_plural = verbose_name
        ordering = ['index','-id']
    def __str__(self):
        # info = "{}--{}--{}".format(self.ssh_user,self.name,self.ssh_port)
        return self.name

class Group(models.Model):
    """ 主机分组 """
    name = models.CharField(max_length=30, verbose_name='分组名称')
    desc = models.CharField(max_length=50, blank=True, null=True, verbose_name="描述")
    # 统计文章数 并放入后台
    def get_items(self):
        return len(self.host_set.all())
    get_items.short_description = '关联数'
    class Meta:
        # db_table = "cmdb_asset_tag"
        verbose_name = '分组'
        verbose_name_plural = verbose_name
    def __str__(self):
        return self.name

class Host(models.Model):
    # 
    asset_type_choice = (
        ('physical_machine', '物理机'),
         ('cloud_host', '云主机'),
        ('cloud_aliyun_host', '阿里云 ECS'),
        ('cloud_tencent_host', '腾讯云CVM'),
        ('virtual_machine', '虚拟机'),
        ('container_node','容器主机节点')
    )
    asset_status_choice = (
        (0, '在线'),
        (1, '下线'),
        (2, '未知'),
        (3, '故障'),
        (4, '备用')
      # {{row.asset_status_display }}  
    )

    id = models.BigAutoField(primary_key=True) # 64位的整型数量更大
    hostname = models.CharField( max_length=64, unique=True, default='',verbose_name='主机名')
    instance_id = models.CharField( max_length=255, blank=True, null=True, verbose_name="实例ID")  
    ip = models.CharField(max_length=20,blank=True,null=True,verbose_name='ip')
    floatingip = models.CharField(max_length=20,blank=True,null=True,verbose_name='弹性ip')
    # ipv6 = models.CharField(max_length=32,blank=True,null=True, verbose_name='ipv6')
   
    ssh_id = models.ForeignKey(
        SSH_account, db_column='ssh_id',blank=True, null=True, on_delete=models.SET_NULL,verbose_name='远程账户')
    region_id = models.ForeignKey(
        Region, db_column='region_id', blank=True, null=True, on_delete=models.SET_NULL,verbose_name='地区')
    platform_id = models.ForeignKey(
        Platform, db_column='platform_id', blank=True, null=True, on_delete=models.SET_NULL,verbose_name='云平台')

    # department = models.ForeignKey('oauth.Departments', null=True, blank=True, on_delete=models.SET_NULL,
    #                                verbose_name='所属部门')

    connect_type = models.SmallIntegerField(default=1,verbose_name="SSH连接方式") # 0.外网 1.内网ip
    asset_type = models.CharField(choices=asset_type_choice, max_length=64, default='cloud_host', verbose_name="类型")
    status = models.SmallIntegerField(choices=asset_status_choice, default=0, verbose_name='设备状态')
    
    os_version = models.CharField(max_length=50, null=True, blank=True,verbose_name="系统")
    cpu = models.CharField(max_length=60, blank=True,null=True)
    memory = models.CharField(max_length=60, blank=True,null=True)
    disk = models.CharField(max_length=60, blank=True,null=True)
    group_id = models.ManyToManyField(
        Group, db_column='group_id', blank=True, verbose_name='主机分组')
    expire_time = models.DateTimeField(blank=True, null=True,default=datetime.datetime(1900,1,1) , verbose_name="租约过期")
    gmt_create = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    gmt_modified = models.DateTimeField(auto_now=True, verbose_name="更新时间")
    desc =  models.CharField(max_length=50, blank=True, null=True, verbose_name="描述")
    class Meta:
        # db_table = 'cmdb_asset_host'
        verbose_name = '主机'
        verbose_name_plural = verbose_name
        ordering = ['-gmt_modified']
        unique_together = (('instance_id', 'hostname'),)
    def __str__(self):
        return self.hostname
