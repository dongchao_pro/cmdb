from django.urls import path,include

from .views import *

from rest_framework.routers import DefaultRouter
router = DefaultRouter()
router.register(r'applications', ApplicationViewSet) 
router.register(r'products', ProductViewSet) 
router.register(r'environments', EnvConfigViewSet) 


urlpatterns = [
    path('env/type/', EnvChoicesAPIView.as_view()),
    path('env/packages/', EnvPackageChoicesAPIView.as_view()),
    path('', include(router.urls)),
]