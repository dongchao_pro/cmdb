from django.db import models
from asset.models import Host
from django.contrib.auth import get_user_model
UserInfo = get_user_model()


class Product(models.Model):
    name = models.CharField(
        db_column='name', max_length=64,default='',verbose_name='名称')
    gmt_create = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    gmt_modified = models.DateTimeField(auto_now=True, verbose_name="编辑时间")
    desc =  models.CharField(max_length=50, blank=True, null=True, verbose_name="描述")
    class Meta:
        ordering = ['id']
        # db_table = 'cmdb_application_product'
        verbose_name = '产品线'
        verbose_name_plural = verbose_name
    def __str__(self):
        return self.name
        

class Application(models.Model):
    name = models.CharField(
        db_column='name', max_length=64,default='',verbose_name='名称')
    repository = models.CharField(max_length=255,null=True, blank=True, verbose_name="git仓库")
    module_name = models.CharField(max_length=255, blank=True, null=True, verbose_name="模块名称")
    app_port = models.CharField(max_length=255, blank=True, null=True, verbose_name="应用端口")
    check_url = models.CharField(max_length=255, blank=True, null=True,verbose_name="检查地址")
    env_id = models.ForeignKey('EnvConfig', db_column='env_id', on_delete=models.SET_NULL, blank=True, null=True)
    prod_id = models.ForeignKey(Product, db_column='prod_id', on_delete=models.SET_NULL, blank=True, null=True)
    developer_id = models.ManyToManyField(UserInfo, db_column='developer_id', related_name='developer', through='AppDeveloper')
    # owner_id = models.ForeignKey(UserInfo, db_column='owner_id', on_delete=models.SET_NULL,related_name='owner', blank=True, null=True)

    host_id = models.ManyToManyField(Host, verbose_name='主机', blank=True)  #
    gmt_create = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    gmt_modified = models.DateTimeField(auto_now=True, verbose_name="编辑时间")
    desc =  models.CharField(max_length=50, blank=True, null=True, verbose_name="描述")

    class Meta:
        ordering = ['id']
        # db_table = 'cmdb_applications'
        verbose_name = '应用集群'
        verbose_name_plural = verbose_name

class AppDeveloper(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=64,default='',verbose_name='名称')
    developer_id = models.ForeignKey(UserInfo, db_column='developer_id', on_delete=models.CASCADE, blank=True, null=True)
    app_id = models.ForeignKey('Application', db_column='app_id', on_delete=models.CASCADE, blank=True, null=True)
    gmt_create = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    gmt_modified = models.DateTimeField(auto_now=True, verbose_name="编辑时间")
    desc =  models.CharField(max_length=50, blank=True, null=True, verbose_name="描述")

    class Meta:
        ordering = ['id']
        # db_table = 'cmdb_application_developer'
        verbose_name = '开发者'
        verbose_name_plural = verbose_name
    def __str__(self):
        return self.name
        

class EnvConfig(models.Model):

    env_choices = {
        ('dev', '开发'),
        ('test', '测试'),
        ('pre', '预发'),
        ('pro', '正式')
    }
    package_choices = {

        ('jar', 'Jar'),
        ('war', 'Tomcat'),
        ('php', 'PHP'),
        ('npm', 'Nodejs'),
        ('vue', 'Vue'),
        ('react', 'React'),
        ('django', 'Django'),
        ('html', '静态HTML'),
        ('file', '静态文件'),
        ('unknown', '未知')
    }

    prometheus_choices = {
        ('open', '开启'),
        ('close', '关闭'),
    }

    skywalking_choices = {
        ('open', '开启'),
        ('close', '关闭'),
    }
    name = models.CharField(max_length=64,default='',verbose_name='名称')
    type = models.CharField(choices=env_choices, max_length=10, verbose_name="环境类型",default='dev')
    package_type = models.CharField(choices=package_choices, max_length=10, verbose_name="包类型",default='unknown')
    local_path = models.CharField(max_length=255, blank=True, null=True, verbose_name="本地路径")
    deploy_path = models.CharField(max_length=255, blank=True, null=True, verbose_name="部署路径")
    log_path = models.CharField(max_length=255, blank=True, null=True, verbose_name="日志路径")
    build_cmd = models.CharField(max_length=255, blank=True, null=True, verbose_name="打包命令")
    start_cmd = models.TextField(blank=True, null=True, verbose_name="启动命令")
    stop_cmd = models.CharField(max_length=255, blank=True, null=True,
                                 verbose_name="停止命令")
    restart_cmd = models.CharField(max_length=255, blank=True, null=True,
                                 verbose_name="重启命令")
    resload_cmd = models.CharField(max_length=255, blank=True, null=True,
                                   verbose_name="重载命令")
    check_cmd = models.CharField(max_length=255, blank=True, null=True,
                                   verbose_name="检查命令")
    prometheus = models.CharField(choices=prometheus_choices,db_column='prometheus', max_length=10, null=True, blank=True,
                                  default='close', verbose_name="prometheus监控")
    
    skywalking = models.CharField(choices=skywalking_choices,db_column='skywalking', max_length=10, null=True, blank=True,default='close',verbose_name="链路跟踪")  # 链路跟踪是否开启（yes/no）
    gmt_create = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    gmt_modified = models.DateTimeField(auto_now=True, verbose_name="编辑时间")
    desc =  models.CharField(max_length=50, blank=True, null=True, verbose_name="描述")

    class Meta:
        ordering = ['id']
        # db_table = 'cmdb_application_env'
        verbose_name = '环境配置'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name